# SASS
## Introduction
This is the git for the presentation about SASS.
You can find here the presentation itself and some code examples.
There are four examples in total
* Pyramid: short example for variables, operator and functions
* Solar-System: short example for mixins
* Lists: short example for loops (for) and conditionals (if/else) 
* Temperatures: bigger example that includes several features 

## Learn SASS
Every example consists of one sass-file, one html-file and the generated css-file.

If you don't want to install anything you can find these examples in my [JSFiddle](https://jsfiddle.net/user/Psyfexx/fiddles/).
If you do so follow these steps:

* First, choose what topic you want to learn (see above) and if you want to use SASS or SCSS (basically indents or brackets)
* Open one of the four examples.
* Now, set the settings to 'Editor Layout' -> tabs(Columns). As you will only work with the SASS/SCSS you won't have to see the other tabs at all times.
* Try to optimize the stylesheet how you see fit. You might want to take a look at the tips in the fiddles or at the [cheetsheet](https://devhints.io/sass).
* Finally you can take a look at my own possible solution. Keep in mind that there are several possible ways to use SASS/SCSS and my own is far from perfect. After all the goal is to make the code more readable and easier to maintain.

## Installation 
If you want to get SASS running in your IDE you have to follow a [few steps](https://dev.to/chrissiemhrk/how-to-setup-sass-in-your-project-2bo1) first

```bash
npm init
npm install node-sass
```
This creates a package.json file and installs the node-sass dependencies 

Note: if the npm command doesn't work you probably have to [install it](https://nodejs.org/en/) first.

In the package.json file you will have to change the line 
```json
"scripts": {
"test": "echo \"Error: no test specified\" && exit 1"
}
```
to 
```json
"scripts": {
"sass": "node-sass -w scss/ -o css/ --recursive"
}
```
Now you only have to start the sass script
```bash
npm run sass
```
With this method you will automatically create a corresponding css file for every sass file you work with. 

It should be mentioned that of course there are other methods to compile your sass file without saving another css file.

## Useful Links 
* [SASS-Cheatsheet](https://devhints.io/sass)
* [SASS-Basics](https://sass-lang.com/guide)
* [German Tutorial](https://www.ionos.de/digitalguide/websites/web-entwicklung/sass-tutorial/)
* [My own tutorial](https://jsfiddle.net/user/Psyfexx/fiddles/)
* [SASS to CSS](https://www.sassmeister.com/) 
